package com.whereta.dao.impl;

import com.whereta.dao.IMenuDao;
import com.whereta.mapper.MenuMapper;
import com.whereta.model.Menu;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by vincent on 15-8-27.
 */
@Repository("menuDao")
public class MenuDaoImpl implements IMenuDao {
    @Resource
    private MenuMapper menuMapper;

    /**
     * 获取所有菜单
     * @return
     */
    public List<Menu> selectAll() {
        return menuMapper.selectAll();
    }

    /**
     * 获取菜单
     * @param menus
     * @param id
     * @return
     */
    public Menu get(List<Menu> menus, int id) {
        for(Menu menu:menus){
            if(menu.getId().intValue()==id){
                return menu;
            }
        }
        return null;
    }

    /**
     * 添加菜单
     * @param menu
     * @return
     */
    public int createMenu(Menu menu) {
        return menuMapper.insertSelective(menu);
    }

    /**
     * 删除菜单
     * @param id
     * @return
     */
    public int deleteMenu(int id) {
        return menuMapper.deleteByPrimaryKey(id);
    }

    /**
     * 更新菜单
     * @param menu
     * @return
     */
    public int updateMenu(Menu menu) {
        return menuMapper.updateByPrimaryKeySelective(menu);
    }
}

package com.whereta.service;

import com.whereta.vo.ResultVO;

/**
 * Created by vincent on 15-9-12.
 */
public interface ILogService {

    ResultVO queryLoginLog(int page, int count);

    ResultVO getAllUserLocations();


}
